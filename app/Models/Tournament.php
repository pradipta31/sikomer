<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tournament extends Model
{
    use HasFactory;

    protected $fillable = [
        'nama_tournament',
        'poster_tournament',
        'tgl_tournament',
        'deskripsi_tournament',
        'history',
        'status'
    ];
}
