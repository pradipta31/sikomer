<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HistoryTournament extends Model
{
    use HasFactory;

    protected $fillable = [
        'tournament_id',
        'daftar_juara'
    ];

    public function tournament(){
        return $this->belongsTo('App\Models\Tournament');
    }
}
