<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RegisterTournament extends Model
{
    use HasFactory;

    protected $fillable = [
        'tournament_id',
        'member_id',
        'nama_team',
        'captain',
        'anggota_1',
        'anggota_2',
        'anggota_3',
        'anggota_4',
        'anggota_5',
        'no_hp',
        'status'
    ];
}
