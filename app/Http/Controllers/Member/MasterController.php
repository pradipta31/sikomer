<?php

namespace App\Http\Controllers\Member;

use Validator;
use Auth;

use App\Models\Gallery;
use App\Models\Event;
use App\Models\User;
use App\Models\Member;
use App\Models\RegisterTournament;
use App\Models\Tournament;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MasterController extends Controller
{
    public function daftarEvent(){
        $events = Event::all();
        return view('member.event.index', compact('events'));
    }

    public function detailEvent($slug){
        $event = Event::where('slug', $slug)->first();
        $galleries = Gallery::where('event_id', $event->id)->get();
        return view('member.event.event', compact('event', 'galleries'));
    }

    public function daftarTournament(){
        $tournaments = Tournament::where('status', 1)->get();
        return view('member.tournament.index', compact('tournaments'));
    }

    public function registerTournament($id){
        $member = Member::where('user_id', Auth::user()->id)->first();
        $register = RegisterTournament::where('member_id', $member->id)->first();
        $reg = RegisterTournament::all();
        $tournament = Tournament::findOrFail($id);
        return view('member.tournament.register', compact('tournament', 'member', 'register', 'reg'));
    }

    public function storeRegister(Request $r){
        $validator = Validator::make($r->all(), [
            'tournament_id' => 'required',
            'user_id' => 'required',
            'nama_team' => 'required',
            'captain' => 'required',
            'anggota_1' => 'required',
            'anggota_2' => 'required',
            'anggota_3' => 'required',
            'anggota_4' => 'required',
            'anggota_5' => 'required',
            'no_hp' => 'required'
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $member = Member::where('user_id', $r->user_id)->first();
            $reg = registerTournament::create([
                'tournament_id' => $r->tournament_id,
                'member_id' => $member->id,
                'nama_team' => $r->nama_team,
                'captain' => $r->captain,
                'anggota_1' => $r->anggota_1,
                'anggota_2' => $r->anggota_2,
                'anggota_3' => $r->anggota_3,
                'anggota_4' => $r->anggota_4,
                'anggota_5' => $r->anggota_5,
                'no_hp' => $r->no_hp,
                'status' => 1
            ]);

            toastr()->success('Data team anda sudah berhasil tersimpan, pastikan nomor Whatsapp anda aktif!');
            return redirect('daftar-tournament');
        }
    }
}
