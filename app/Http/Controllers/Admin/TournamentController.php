<?php

namespace App\Http\Controllers\Admin;

use Validator;
use Image;

use App\Models\Tournament;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TournamentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tournaments = Tournament::all();
        $no = 1;
        return view('admin.tournament.index', compact('tournaments', 'no'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.tournament.tournament');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        $validator = Validator::make($r->all(), [
            'nama_tournament' => 'required',
            'poster_tournament' => 'required|image|mimes:jpeg,png,jpg|max:5024',
            'tgl_tournament' => 'required',
            'deskripsi_tournament' => 'required',
            'status' => 'required'
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $status = '';
            if($r->status == 1){
                $status = 'aktif';
            }else{
                $status = 'tidak aktif';
            }
            $poster_tournament = $r->file('poster_tournament');
            $filename = time() . '.' . $poster_tournament->getClientOriginalExtension();
            Image::make($poster_tournament)->save(public_path('/images/poster/'.$filename));
            $tournaments = Tournament::create([
                'nama_tournament' => $r->nama_tournament,
                'poster_tournament' => $filename,
                'tgl_tournament' => $r->tgl_tournament,
                'deskripsi_tournament' => $r->deskripsi_tournament,
                'history' => 0,
                'status' => $r->status
            ]);

            toastr()->success('Turnamen baru berhasil ditambahkan dengan status '.$status);
            return redirect('admin/turnamen');
        }
    }

    public function changeFoto(Request $r, $id){
        $validator = Validator::make($r->all(), [
            'poster_tournament' => 'required|image|mimes:jpeg,png,jpg|max:5024'
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $poster_tournament = $r->file('poster_tournament');
            $filename = time() . '.' . $poster_tournament->getClientOriginalExtension();
            $find_img = Tournament::findOrFail($id);
            unlink(public_path('/images/poster/').$find_img->poster_tournament);
            Image::make($poster_tournament)->save(public_path('/images/poster/'.$filename));
            $find_img->update([
                'poster_tournament' => $filename
            ]);

            toastr()->success('Poster turnamen berhasil diganti!');
            return redirect('admin/turnamen');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Tournament $dataTournament)
    {
        return view('admin.tournament.tournament', compact('dataTournament'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, Tournament $dataTournament)
    {
        $validator = Validator::make($r->all(), [
            'nama_tournament' => 'required',
            'tgl_tournament' => 'required',
            'deskripsi_tournament' => 'required',
            'status' => 'required'
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $tournaments = Tournament::where('id', $dataTournament->id)->update([
                'nama_tournament' => $r->nama_tournament,
                'tgl_tournament' => $r->tgl_tournament,
                'deskripsi_tournament' => $r->deskripsi_tournament,
                'status' => $r->status
            ]);

            toastr()->success('Data turnamen berhasil di ubah!');
            return redirect('admin/turnamen');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tournament = Tournament::where('id', $id)->delete();
        toastr()->success('Data Tournament berhasil dihapus!');
        return redirect('admin/turnamen');
    }
}
