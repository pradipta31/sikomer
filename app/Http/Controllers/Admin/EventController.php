<?php

namespace App\Http\Controllers\Admin;

use Validator;
use Image;
use App\Models\Gallery;
use App\Models\Event;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::all();
        $no = 1;
        return view('admin.event.index', compact('events', 'no'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.event.event');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        $validator = Validator::make($r->all(), [
            'nama_event' => 'required',
            'cover' => 'required|image|mimes:jpeg,png,jpg|max:5024',
            'tanggal' => 'required',
            'deskripsi' => 'required'
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $cover = $r->file('cover');
            $filename = time() . '.' . $cover->getClientOriginalExtension();
            Image::make($cover)->save(public_path('/images/event/'.$filename));
            $event = Event::create([
                'nama_event' => $r->nama_event,
                'slug' => str_slug($r->nama_event),
                'cover' => $filename,
                'tanggal' => $r->tanggal,
                'deskripsi' => $r->deskripsi
            ]);

            toastr()->success('Event baru berhasil ditambahkan');
            return redirect('admin/event');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function changeFoto(Request $r, $id){
        $validator = Validator::make($r->all(), [
            'cover' => 'required|image|mimes:jpeg,png,jpg|max:5024'
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $cover = $r->file('cover');
            $filename = time() . '.' . $cover->getClientOriginalExtension();
            $event = Event::findOrFail($id);
            unlink(public_path('/images/event/').$event->cover);
            Image::make($cover)->save(public_path('/images/event/'.$filename));
            $event->update([
                'cover' => $filename
            ]);

            toastr()->success('Cover Event berhasil diganti!');
            return redirect('admin/event');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $dataEvent)
    {
        return view('admin.event.event', compact('dataEvent'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, Event $dataEvent)
    {
        $validator = Validator::make($r->all(), [
            'nama_event' => 'required',
            'tanggal' => 'required',
            'deskripsi' => 'required'
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $event = Event::where('id', $dataEvent->id)->update([
                'nama_event' => $r->nama_event,
                'slug' => str_slug($r->nama_event),
                'tanggal' => $r->tanggal,
                'deskripsi' => $r->deskripsi
            ]);

            toastr()->success('Event berhasil diubah!');
            return redirect('admin/event');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = Event::where('id', $id)->first();
        $gallery = Gallery::where('event_id', $event->id)->first();
        if($gallery != NULL){
            $gallery->delete();
        }
        unlink(public_path('/images/event/').$event->cover);

        $event->delete();
        
        toastr()->success('Data Event berhasil dihapus!');
        return redirect('admin/event');
    }
}
