<?php

namespace App\Http\Controllers\Admin;

use Validator;
use App\Models\Tournament;
use App\Models\HistoryTournament;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tournaments = Tournament::where('status', 0)
        ->where('history', 0)
        ->paginate(6);
        $histories = HistoryTournament::all();
        $no = 1;
        $num = 1;
        return view('admin.history.index', compact('histories', 'tournaments', 'no', 'num'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $tournament = Tournament::where('id',$id)->first();
        return view('admin.history.history', compact('tournament'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        $validator = Validator::make($r->all(), [
            'tournament_id' => 'required',
            'daftar_juara' => 'required'
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $history = HistoryTournament::create([
                'tournament_id' => $r->tournament_id,
                'daftar_juara' => $r->daftar_juara
            ]);

            $tournament = Tournament::where('id', $r->tournament_id)->update([
                'history' => 1
            ]);

            toastr()->success('History turnament Berhasil ditambahkan!');
            return redirect('admin/history');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(HistoryTournament $dataHistory)
    {
        return view('admin.history.history', compact('dataHistory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, HistoryTournament $dataHistory)
    {
        $validator = Validator::make($r->all(), [
            'daftar_juara' => 'required',
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $history = HistoryTournament::where('id', $dataHistory->id)->update([
                'daftar_juara' => $r->daftar_juara,
            ]);

            toastr()->success('Data History Turnamen berhasil di ubah!');
            return redirect('admin/history');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $history = HistoryTournament::where('id', $id)->first();
        $tournament = Tournament::where('id', $history->tournament_id)->update([
            'history' => 0
        ]);
        $history->delete();
        toastr()->success('Data History Tournament berhasil dihapus!');
        return redirect('admin/history');
    }
}
