<?php

namespace App\Http\Controllers\Admin;

use Validator;
use Auth;
use Hash;
use Illuminate\Validation\Rule;

use App\Models\User;
use App\Models\Member;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = Member::all();
        $no = 1;
        return view('admin.member.index', compact('users', 'no'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.member.member');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        $validator = Validator::make($r->all(), [
            'nama' => 'required',
            'username' => 'required',
            'email' => 'required',
            'password' => 'required',
            'role' => 'required',
            'status' => 'required',
            'nickname' => 'required',
            'tier' => 'required',
            'alamat' => 'required',
            'no_telp' => 'required'
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $user = User::create([
                'nama' => $r->nama,
                'username' => $r->username,
                'email' => $r->email,
                'password' => Hash::make($r->password),
                'role' => $r->role,
                'status' => $r->status,
            ]);

            $member = Member::create([
                'user_id' => $user->id,
                'nickname' => $r->nickname,
                'tier' => $r->tier,
                'alamat' => $r->alamat,
                'no_hp' => $r->no_telp
            ]);

            try{
                \Mail::send('email', [
                    'nama' => $r->nama,
                    'username' => $r->username,
                    'email' => $r->email,
                    'password' => $r->password
                ], function ($message) use ($r)
                {
                    $message->subject('Hi! '.$r->nama);
                    $message->from('aovbalicommunity@gmail.com', 'COMMUNITY AOV BALI');
                    $message->to($r->email);
                });
                toastr()->success('Email berhasil terkirim ke '.$r->email.' Harap untuk konfirmasi email', '', ['timeOut' => 5000]);
                return redirect('admin/member');
            }
            catch (Exception $e){
                return response (['status' => false,'errors' => $e->getMessage()]);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, User $dataMember)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Member $dataMember)
    {
        return view('admin.member.member', compact('dataMember'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, Member $dataMember)
    {
        // dd($r->dataMember);
        $validator = Validator::make($r->all(), [
        // $rules = [
            'nama' => 'required',
            'username' => 'required',
            // 'email' => ['required', Rule::unique('users')->ignore($dataMember->user_id,'email')],
            'email' => 'required',
            'role' => 'required',
            'status' => 'required',
            'nickname' => 'required',
            'tier' => 'required',
            'alamat' => 'required',
            'no_telp' => 'required'
        // ];

        // $customMessages = [
        //     'required' => 'The :attribute field is required.'
        // ];

        // $this->validate($r, $rules, $customMessages);
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            if ($r->password == null) {
                $user = User::where('id', $dataMember->id)->update([
                    'nama' => $r->nama,
                    'username' => $r->username,
                    'email' => $r->email,
                    'role' => $r->role,
                    'status' => $r->status,
                ]);
            }else{
                $user = User::where('id', $dataMember->id)->update([
                    'nama' => $r->nama,
                    'username' => $r->username,
                    'email' => $r->email,
                    'password' => Hash::make($r->password),
                    'role' => $r->role,
                    'status' => $r->status,
                ]);

                try{
                    \Mail::send('email', [
                        'nama' => $r->nama,
                        'username' => $r->username,
                        'email' => $r->email,
                        'password' => $r->password
                    ], function ($message) use ($r)
                    {
                        $message->subject('Hi! '.$r->nama);
                        $message->from('aovbalicommunity@gmail.com', 'COMMUNITY AOV BALI');
                        $message->to($r->email);
                    });
                }
                catch (Exception $e){
                    return response (['status' => false,'errors' => $e->getMessage()]);
                }
            }

            $member = Member::where('user_id', $dataMember->user_id)->update([
                'nickname' => $r->nickname,
                'tier' => $r->tier,
                'alamat' => $r->alamat,
                'no_hp' => $r->no_telp
            ]);

            toastr()->success('Data member berhasil diubah!');
            return redirect(url('admin/member'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $member = Member::where('id',$id)->first();
        $user = User::where('id', $member->user_id)->delete();
        $member->delete();
        toastr()->success('Data Member berhasil dihapus!');
        return redirect('admin/member');
    }
}
