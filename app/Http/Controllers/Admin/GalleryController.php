<?php

namespace App\Http\Controllers\Admin;

use Validator;
use Image;
use Storage;

use App\Models\Event;
use App\Models\Gallery;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($slug)
    {
        $no = 1;
        $event = Event::where('slug',$slug)->first();
        $galleries = Gallery::all();
        return view('admin.event.gallery', compact('event', 'galleries', 'no'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        // $validator = Validator::make($r->all(), [
        //     'photo' => 'required|image|mimes:jpeg,png,jpg|max:5024'
        // ]);
        // if (!$validator->fails()) {
            if($r->hasFile('photo')){
                $allowedfileExtension=['jpg','png','jpeg'];
                $files = $r->file('photo');
                foreach($files as $file){
                    $filename = time() . '.' . $file->getClientOriginalName();
                    $extension = $file->getClientOriginalExtension();
                    $check=in_array($extension,$allowedfileExtension);
                    if($check){
                        $loc = $file->store('public/gallery');
                        Gallery::create([
                            'event_id' => $r->id_event,
                            'photo' => $loc
                        ]);
                    }
                }
                toastSuccess('Photo berhasil ditambahkan.');
                return redirect()->back();
            }
        // }
    
        // }else{
        //     toastr()->error($validator->messages()->first());
        //     return redirect()->back()->withInput();
        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gallery = Gallery::where('id', $id)->first();
        Storage::delete($gallery->photo);
        $gallery->delete();

        toastSuccess('Photo berhasil dihapus!');
        return redirect()->back();
    }
}
