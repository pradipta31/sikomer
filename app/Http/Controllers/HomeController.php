<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\Member;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (Auth::user()->role == 'admin') {
            toastr()->success('Selamat datang '.Auth::user()->nama);
            return view('admin.dashboard');
        }else if(Auth::user()->role == 'member'){
            $member = Member::where('user_id', Auth::user()->id)->first();
            toastr()->success('Selamat datang '.Auth::user()->nama);
            return view('member.home.index', compact('member'));
        }
    }
}
