<?php

namespace App\Http\Controllers;


use Validator;
use Auth;
use Hash;

use App\Models\User;
use App\Models\Member;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('auth.daftar');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        $validator = Validator::make($r->all(), [
            'nama' => 'required',
            'username' => 'required',
            'email' => 'required',
            'password' => 'required',
            'nickname' => 'required',
            'tier' => 'required',
            'alamat' => 'required',
            'no_hp' => 'required'
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $user = User::create([
                'nama' => $r->nama,
                'username' => $r->username,
                'email' => $r->email,
                'password' => Hash::make($r->password),
                'role' => 'member',
                'status' => 1,
            ]);

            $member = Member::create([
                'user_id' => $user->id,
                'nickname' => $r->nickname,
                'tier' => $r->tier,
                'alamat' => $r->alamat,
                'no_hp' => $r->no_hp
            ]);

            try{
                \Mail::send('email', [
                    'nama' => $r->nama,
                    'username' => $r->username,
                    'email' => $r->email,
                    'password' => $r->password
                ], function ($message) use ($r)
                {
                    $message->subject('Hi! '.$r->nama);
                    $message->from('aovbalicommunity@gmail.com', 'COMMUNITY AOV BALI');
                    $message->to($r->email);
                });
                toastr()->success('Email berhasil terkirim ke '.$r->email.' Harap untuk konfirmasi email', '', ['timeOut' => 5000]);
                return redirect('login');
            }
            catch (Exception $e){
                return response (['status' => false,'errors' => $e->getMessage()]);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
