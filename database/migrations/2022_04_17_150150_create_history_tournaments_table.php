<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_tournaments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('tournament_id')->constrained()->onUpdate('cascade')->onDelete('cascade');
            $table->text('daftar_juara');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_tournaments');
    }
};
