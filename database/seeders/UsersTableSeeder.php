<?php

namespace Database\Seeders;
use DB;
use Hash;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'nama' => 'Admin',
                'username' => 'admin',
                'email' => 'admin@gmail.com',
                'password' => Hash::make('123'),
                'role' => 'admin',
                'status' => 1,
                'created_at' => NOW(),
                'updated_at' => NOW()
            ],
            // [
            //     'nama' => 'Member',
            //     'username' => 'member',
            //     'email' => 'member@gmail.com',
            //     'password' => Hash::make('123'),
            //     'role' => 'member',
            //     'status' => 1,
            //     'created_at' => NOW(),
            //     'updated_at' => NOW()
            // ],
        ]);
    }
}
