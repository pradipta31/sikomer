<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('member.home');
});

Auth::routes();
Auth::routes(['register' => false, 'verify' => true]);

Route::get('/admin', function(){
    return redirect('/home');
});

Route::get('member/daftar', [App\Http\Controllers\RegisterController::class, 'index']);
Route::post('member/daftar', [App\Http\Controllers\RegisterController::class, 'store']);
Route::get('/daftar-event', [App\Http\Controllers\Member\MasterController::class, 'daftarEvent']);
Route::get('/daftar-event/{slug}', [App\Http\Controllers\Member\MasterController::class, 'detailEvent']);

Route::group(['middleware' => 'auth'], function(){
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('daftar-tournament', [App\Http\Controllers\Member\MasterController::class, 'daftarTournament']);
    Route::get('daftar-tournament/daftar/{id}', [App\Http\Controllers\Member\MasterController::class, 'registerTournament']);
    Route::post('daftar-tournament/daftar', [App\Http\Controllers\Member\MasterController::class, 'storeRegister']);

    Route::group(['prefix' => 'admin'], function(){
        // ROUTE MEMBER
        Route::get('/member', [App\Http\Controllers\Admin\MemberController::class, 'index']);
        Route::get('/member/tambah', [App\Http\Controllers\Admin\MemberController::class, 'create'])->name('create:member');
        Route::post('/member/tambah', [App\Http\Controllers\Admin\MemberController::class, 'store'])->name('store:member');
        Route::get('/member/edit/{dataMember}', [App\Http\Controllers\Admin\MemberController::class, 'edit'])->name('edit:member');
        Route::put('/member/edit/{dataMember}', [App\Http\Controllers\Admin\MemberController::class, 'update'])->name('update:member');
        Route::delete('/member/delete/{id}', [App\Http\Controllers\Admin\MemberController::class, 'destroy']);

        //ROUTE TOURNAMENT
        Route::get('/turnamen', [App\Http\Controllers\Admin\TournamentController::class, 'index']);
        Route::put('/turnamen/{id}/ganti-poster', [App\Http\Controllers\Admin\TournamentController::class, 'changeFoto']);
        Route::get('/turnamen/tambah', [App\Http\Controllers\Admin\TournamentController::class, 'create'])->name('create:tournament');
        Route::post('/turnamen/tambah', [App\Http\Controllers\Admin\TournamentController::class, 'store'])->name('store:tournament');
        Route::get('/turnamen/edit/{dataTournament}', [App\Http\Controllers\Admin\TournamentController::class, 'edit'])->name('edit:tournament');
        Route::put('/turnamen/edit/{dataTournament}', [App\Http\Controllers\Admin\TournamentController::class, 'update'])->name('update:tournament');
        Route::delete('/turnamen/delete/{id}', [App\Http\Controllers\Admin\TournamentController::class, 'destroy']);

        //ROUTE REGISTER TOURNAMENT
        Route::get('/register', [App\Http\Controllers\Admin\RegisterController::class, 'index']);
        Route::get('/register/{id}', [App\Http\Controllers\Admin\RegisterController::class, 'show']);

        //ROUTE HISTORY TOURNAMENT
        Route::get('/history', [App\Http\Controllers\Admin\HistoryController::class, 'index']);
        Route::get('/history/tambah{id}', [App\Http\Controllers\Admin\HistoryController::class, 'create'])->name('create:history');
        Route::post('/history/tambah', [App\Http\Controllers\Admin\HistoryController::class, 'store'])->name('store:history');
        Route::get('/history/edit/{dataHistory}', [App\Http\Controllers\Admin\HistoryController::class, 'edit'])->name('edit:history');
        Route::put('/history/edit/{dataHistory}', [App\Http\Controllers\Admin\HistoryController::class, 'update'])->name('update:history');
        Route::delete('/history/delete/{id}', [App\Http\Controllers\Admin\HistoryController::class, 'destroy']);

        // ROUTE EVENT
        Route::get('/event', [App\Http\Controllers\Admin\EventController::class, 'index']);
        Route::put('/event/{id}/ganti-cover', [App\Http\Controllers\Admin\EventController::class, 'changeFoto']);
        Route::get('/event/tambah', [App\Http\Controllers\Admin\EventController::class, 'create'])->name('create:event');
        Route::post('/event/tambah', [App\Http\Controllers\Admin\EventController::class, 'store'])->name('store:event');
        Route::get('/event/edit/{dataEvent}', [App\Http\Controllers\Admin\EventController::class, 'edit'])->name('edit:event');
        Route::put('/event/edit/{dataEvent}', [App\Http\Controllers\Admin\EventController::class, 'update'])->name('update:event');
        Route::delete('/event/delete/{id}', [App\Http\Controllers\Admin\EventController::class, 'destroy']);

        // ROUTE GALLERY
        Route::get('/event/{slug}/gallery', [App\Http\Controllers\Admin\GalleryController::class, 'index']);
        Route::post('/event/gallery', [App\Http\Controllers\Admin\GalleryController::class, 'store']);
        Route::delete('/gallery/delete/{id}', [App\Http\Controllers\Admin\GalleryController::class, 'destroy']);
    });
    
});

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
