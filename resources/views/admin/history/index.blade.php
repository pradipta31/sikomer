@extends('admin.layouts.master',['activeMenu' => 'history'])
@section('title','Data History Turnamen')
@section('breadcrumb', 'Data History Turnamen')
@section('detail_breadcrumb', 'Manajemen Data History Turnamen')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
    @include('admin.layouts.breadcrumb')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <a href="#" class="btn btn-md btn-primary" style="margin-bottom: 10px" data-toggle="modal" data-target="#createHistory">
                            <i class="fa fa-plus"></i>
                            Tambah History Turnamen
                        </a>
                        <div class="modal fade" id="createHistory" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h3 class="modal-title" id="exampleModalLabel">PILIH TURNAMEN</h3>
                                        <small>Note: Hanya Turnamen yang telah selesai dapat ditambahkan history turnamen!</small>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama Turnamen</th>
                                                        <th>Tanggal Turnamen</th>
                                                        <th>Status</th>
                                                        <th>Opsi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($tournaments as $item)
                                                        <tr>
                                                            <td>{{$no++}}</td>
                                                            <td>{{$item->nama_tournament}}</td>
                                                            <td><span class="label label-primary">{{date('d M Y', strtotime($item->tgl_tournament))}}</span></td>
                                                            <td>
                                                                @if ($item->status == 1)
                                                                    <span class="label label-success">Aktif</span>
                                                                @else
                                                                    <span class="label label-success">Selesai</span>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                <a href="{{ route('create:history', $item->id) }}" class="btn btn-sm btn-primary">
                                                                    <i class="fa fa-plus"></i> History
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table id="tableTurnamen" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Turnamen</th>
                                        <th>Tanggal Turnamen</th>
                                        <th>Daftar Juara</th>
                                        <th>Status</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($histories as $history)
                                        <tr>
                                            <td>{{$num++}}</td>
                                            <td>{{$history->tournament->nama_tournament}}</td>
                                            <td><span class="label label-primary">{{date('d M Y', strtotime($history->tournament->tgl_tournament))}}</span></td>
                                            <td>
                                                <a href="javascript:void(0);" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#daftarJuara{{$history->id}}">
                                                    <i class="fa fa-eye"></i> Lihat Daftar Juara
                                                </a>
                                            </td>
                                            <td>
                                                @if ($history->tournament->status == 1)
                                                    <span class="label label-success">Aktif</span>
                                                @else
                                                    <span class="label label-success">Selesai</span>
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{route('edit:history', $history)}}" class="btn btn-sm btn-warning">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                                <a href="javascript:void(0);" class="btn btn-danger btn-sm" onclick="deleteHistory({{ $history->id }})">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </td>
                                        </tr>

                                        <div class="modal fade" id="daftarJuara{{$history->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h3 class="modal-title" id="exampleModalLabel">NAMA Tournament : {{$history->tournament->nama_tournament}}</h3>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <form action="#" method="post" enctype="multipart/form-data">
                                                        <div class="modal-body">
                                                            {{$history->daftar_juara}}
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <form class="hidden" action="" method="post" id="formDelete">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="delete">
    </form>
@endsection

@section('js')
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tableTurnamen').dataTable()
        });

        function deleteHistory(id){
            swal({
                title: "Anda yakin?",
                text: "Data History Tournament akan terhapus secara permanen!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    swal("Berhasil! Data Tournament yang anda pilih berhasil terhapus!", {
                        icon: "success",
                    }).then((res) => {
                        $('#formDelete').attr('action', '{{url('admin/history/delete/')}}/'+id);
                        $('#formDelete').submit();
                    }); 
                }
            });
        }
    </script>
@endsection
