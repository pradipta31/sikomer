@extends('admin.layouts.master',['activeMenu' => 'history'])
@section('title', isset($dataHistory) ? 'Edit History' : 'Tambah History')
@section('breadcrumb', isset($dataHistory) ? 'Edit History' : 'Tambah History')
@section('detail_breadcrumb', isset($dataHistory) ? 'Edit History' : 'Tambah History')
@section('content')
    @include('admin.layouts.breadcrumb')
    <style>
        select option[disabled] {
            display: none;
        }
    </style>
    <section class="content">
        <form action="{{ isset($dataHistory) ? route('update:history', $dataHistory) : route('store:history') }}" 
        name="formUser" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            {{ isset($dataHistory) ? method_field('PUT') : ''}}
            <input type="hidden" name="tournament_id" value="{{$dataHistory->tournament_id}}">
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">{{ isset($dataHistory) ? 'Edit History' : 'Tambah History Baru '.$tournament->nama_tournament }}</h3>
                        </div>
                        <div class="box-body">
                            @if (isset($dataHistory))
                                <div class="form-group">
                                    <label for="">Nama Turnamen</label>
                                    <input type="text" name="nama_tournament" class="form-control"  placeholder="Masukan Nama Turnamen"
                                    value="{{ isset($dataHistory) ? $dataHistory->tournament->nama_tournament : '' }}" readonly>
                                </div>
                            @else
                                <div class="form-group">
                                    <label for="">Nama Turnamen</label>
                                    <input type="text" name="nama_tournament" class="form-control"  placeholder="Masukan Nama Turnamen"
                                    value="{{ isset($dataHistory) ? $dataHistory->tournament->nama_tournament : $tournament->nama_tournament }}" readonly>
                                </div>
                            @endif
                            <div class="form-group">
                                <label for="">Daftar Juara</label>
                                <textarea name="daftar_juara" cols="30" rows="10" class="form-control">
                                    {{ isset($dataHistory) ? $dataHistory->daftar_juara : old('daftar_juara') }}
                                </textarea>
                            </div>
                        </div>
                        <div class="box-footer">
                            <a href="{{url('admin/history')}}" class="btn btn-default">Kembali</a>
                            <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection