@extends('admin.layouts.master',['activeMenu' => 'dashboard'])
@section('title','Dashboard')
@section('breadcrumb', 'Dashboard')
@section('detail_breadcrumb', 'Control Panel')
@section('content')
    @include('admin.layouts.breadcrumb')
    @php
        $role = '';
        if(Auth::user()->role == 'admin'){
            $role = 'admin';
        }else{
            $role = 'member';
        }
        
    @endphp
    <section class="content">
        <div class="row">
            <div class="col-lg-12 col-xs-12 col-md-12">
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-check"></i> Selamat datang {{Auth::user()->nama}}</h4>
                    Pada halaman dashboard anda dapat melihat beberapa informasi mengenai website anda.
                </div>
            </div>
            <div class="col-lg-4 col-xs-6">
                <a href="{{url($role.'/barang')}}">
                    <div class="small-box bg-navy">
                        <div class="inner">
                            <h3>1122</h3>
            
                            <p>Jumlah Barang</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-clone"></i>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-xs-6">
                @if (Auth::user()->role == 'admin')
                    <a href="{{url($role.'/instansi')}}">    
                @else
                    <a href="#">
                @endif
                    <div class="small-box bg-olive">
                        <div class="inner">
                            <h3>1122</h3>
            
                            <p>Jumlah Instansi</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-building"></i>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-xs-6">
                @if (Auth::user()->role == 'admin')
                    <a href="{{url($role.'/golongan')}}">    
                @else
                    <a href="#">
                @endif
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3>1122</h3>
            
                            <p>Jumlah Golongan</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-external-link"></i>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-lg-4 col-xs-6">
                <a href="{{url($role.'/penitipan')}}">
                    <div class="small-box bg-blue">
                        <div class="inner">
                            <h3>1122</h3>
            
                            <p>Jumlah Penitipan</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-hourglass-half"></i>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-xs-6">
                <a href="{{url($role.'/mutasi')}}">
                    <div class="small-box bg-purple">
                        <div class="inner">
                            <h3>1122</h3>
            
                            <p>Jumlah Mutasi</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-exchange"></i>
                        </div>
                    
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-xs-6">
                <a href="{{url($role.'/pengeluaran')}}">
                    <div class="small-box bg-red">
                        <div class="inner">
                            <h3>1122</h3>
            
                            <p>Jumlah Barang Keluar</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-trash"></i>
                        </div>
                    
                    </div>
                </a>
            </div>
        </div>
    </section>
@endsection
