<section class="content-header">
    <h1>
        @yield('breadcrumb')
        <small>@yield('detail_breadcrumb')</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">@yield('breadcrumb')</li>
    </ol>
</section>