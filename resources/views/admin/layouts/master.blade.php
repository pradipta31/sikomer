<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SIDABAR | @yield('title')</title>
    <link rel="shortcut icon" href="{{asset('logo/icon.png')}}" type="image/x-icon" />
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <link rel="stylesheet" href="{{asset('backend/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/Ionicons/css/ionicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/dist/css/AdminLTE.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/dist/css/skins/_all-skins.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/plugins/pace/pace.css')}}">
    @toastr_css

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    @yield('css')
</head>
<body class="hold-transition skin-blue sidebar-mini">
    <script src="{{url('backend/plugins/pace/pace.min.js')}}"></script>
    <div class="wrapper">
        @include('admin.layouts.header')
        @include('admin.layouts.navigation', ['activeMenu' => $activeMenu])
        <div class="content-wrapper">
            @yield('content')
        </div>
        @include('admin.layouts.footer')
    </div>
    
    <script src="{{asset('backend/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/jquery-ui/jquery-ui.min.js')}}"></script>
    <script>
        $.widget.bridge('uibutton', $.ui.button);
    </script>
    <script src="{{asset('backend/plugins/sweetalert/sweetalert.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/fastclick/lib/fastclick.js')}}"></script>
    <script src="{{asset('backend/dist/js/adminlte.js')}}"></script>
    <script src="{{asset('backend/dist/js/pages/dashboard.js')}}"></script>
    <script src="{{asset('backend/dist/js/demo.js')}}"></script>
    <script type="text/javascript">
        function saveBtn(res){
            // res.disabled = true;
            res.innerHTML = '<i class="fa fa-spinner fa-spin"></i> Loading';
        }
    </script>
    @jquery
    @toastr_js
    @toastr_render
    @yield('js')
</body>
</html>
