<li class="header">MEMBER NAVIGATION</li>
<li class="treeview {{$activeMenu == 'member' ? 'active' : ''}}">
    <a href="#">
        <i class="fa fa-users"></i>
        <span>Data Member</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{url('admin/member/tambah')}}"><i class="fa fa-plus"></i> Tambah Member</a></li>
        <li><a href="{{url('admin/member')}}"><i class="fa fa-circle-o"></i> Data Member</a></li>
    </ul>
</li>
<li class="header">MASTER NAVIGATION</li>
<li class="treeview {{$activeMenu == 'tournaments' ? 'active' : ''}}">
    <a href="#">
        <i class="fa fa-trophy"></i>
        <span>Data Turnamen</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{url('admin/turnamen/tambah')}}"><i class="fa fa-plus"></i> Tambah Turnamen</a></li>
        <li><a href="{{url('admin/turnamen')}}"><i class="fa fa-circle-o"></i> Data Turnamen</a></li>
    </ul>
</li>

<li class="{{$activeMenu == 'register' ? 'active' : ''}}"><a href="{{url('admin/register')}}"><i class="fa fa-registered"></i> <span>Registrasi Turnamen</span></a></li>

<li class="{{$activeMenu == 'history' ? 'active' : ''}}"><a href="{{url('admin/history')}}"><i class="fa fa-history"></i> <span>History Turnamen</span></a></li>

<li class="header">GALLERY NAVIGATION</li>
<li class="treeview {{$activeMenu == 'event' ? 'active' : ''}}">
    <a href="#">
        <i class="fa fa-calendar-check-o"></i>
        <span>Gallery Events</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{url('admin/event/tambah')}}"><i class="fa fa-plus"></i> Tambah Event</a></li>
        <li><a href="{{url('admin/event')}}"><i class="fa fa-circle-o"></i> Data Event</a></li>
    </ul>
</li>