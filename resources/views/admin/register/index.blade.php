@extends('admin.layouts.master',['activeMenu' => 'register'])
@section('title','Data Registrasi Peserta')
@section('breadcrumb', 'Data Registrasi Peserta')
@section('detail_breadcrumb', 'Manajemen Data Registrasi Peserta')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
    @include('admin.layouts.breadcrumb')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="tableTurnamen" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Turnamen</th>
                                        <th>Poster Turnamen</th>
                                        <th>Tanggal Turnamen</th>
                                        <th>List Peserta</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($tournaments as $t)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{$t->nama_tournament}}</td>
                                            <td>
                                                <a href="#" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#showImage{{$t->id}}">
                                                    Lihat Foto
                                                </a>
                                            </td>
                                            <td><span class="label label-primary">{{date('d M Y', strtotime($t->tgl_tournament))}}</span></td>
                                            <td>
                                                <a href="{{url('admin/register/'.$t->id)}}" class="btn btn-success btn-sm"><i class="fa fa-users"></i> Daftar Peserta >></a>
                                            </td>
                                            <td>
                                                @if ($t->status == 1)
                                                    <span class="label label-success">Aktif</span>
                                                @else
                                                    <span class="label label-success">Selesai</span>
                                                @endif
                                            </td>
                                        </tr>
                                        <div class="modal fade" id="showImage{{$t->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <form action="#" method="POST" enctype="multipart/form-data">
                                                        <div class="modal-body">
                                                            <img src="{{asset('images/poster/'.$t->poster_tournament)}}" alt="" class="img-responsive img-fluid">
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tableTurnamen').dataTable()
        });
    </script>
@endsection
