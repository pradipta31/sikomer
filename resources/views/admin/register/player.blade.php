@extends('admin.layouts.master', ['activeMenu' => 'register'])
@section('title', 'Data Peserta Tournament')
@section('breadcrumb', 'Data Peserta Tournament')
@section('detail_breadcrumb', 'Manajemen Data Peserta Tournament ' . $tournament->nama_tournament)
@section('css')
    <link rel="stylesheet" href="{{ asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/bower_components/select2/dist/css/select2.min.css') }}">
@endsection
@section('content')
    @include('admin.layouts.breadcrumb')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="tablePeserta" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Team</th>
                                        <th>Kapten</th>
                                        <th>Anggota</th>
                                        <th>No HP Kapten</th>
                                        {{-- <th>Opsi</th> --}}
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($registers as $register)
                                        <tr>
                                            <td>{{ $no++ }}</td>
                                            <td>{{ $register->nama_team }}</td>
                                            <td>{{ $register->captain }}</td>
                                            <td>
                                                <a href="#" class="btn btn-primary btn-sm" data-toggle="modal"
                                                    data-target="#showAnggota{{ $register->id }}">
                                                    List Anggota
                                                </a>
                                            </td>
                                            <td>{{ $register->no_hp }}</td>
                                        </tr>
                                        <div class="modal fade" id="showAnggota{{ $register->id }}" tabindex="-1"
                                            role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h3 class="modal-title">List Anggota Team : {{$register->nama_team}}</h3>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p style="font-size: 19px">
                                                            - {{$register->captain}} <span class="label label-primary">Kapten</span> <br>
                                                            1. {{$register->anggota_1}} <br>
                                                            2. {{$register->anggota_2}} <br>
                                                            3. {{$register->anggota_3}} <br>
                                                            4. {{$register->anggota_4}} <br>
                                                            5. {{$register->anggota_5}} <br>
                                                        </p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="{{ asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript">
        $(function() {
            $('#tablePeserta').dataTable()
        });
    </script>
@endsection
