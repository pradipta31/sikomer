@extends('admin.layouts.master',['activeMenu' => 'tournaments'])
@section('title', isset($dataTournament) ? 'Edit Tournament' : 'Tambah Tournament')
@section('breadcrumb', isset($dataTournament) ? 'Edit Tournament' : 'Tambah Tournament')
@section('detail_breadcrumb', isset($dataTournament) ? 'Edit Tournament' : 'Tambah Tournament')
@section('content')
    @include('admin.layouts.breadcrumb')
    <style>
        select option[disabled] {
            display: none;
        }
    </style>
    <section class="content">
        <form action="{{ isset($dataTournament) ? route('update:tournament', $dataTournament) : route('store:tournament') }}" 
        name="formUser" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            {{ isset($dataTournament) ? method_field('PUT') : ''}}
            <input type="hidden" name="length" value="6">
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">{{ isset($dataTournament) ? 'Edit Turnamen' : 'Tambah Turnamen Baru' }}</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="">Nama Turnamen</label>
                                <input type="text" name="nama_tournament" class="form-control"  placeholder="Masukan Nama Turnamen"
                                value="{{ isset($dataTournament) ? $dataTournament->nama_tournament : old('nama_tournament') }}">
                            </div>
                            <div class="form-group">
                                <label for="">Tanggal Turnamen</label>
                                <input type="date" name="tgl_tournament" class="form-control"
                                 value="{{ isset($dataTournament) ? $dataTournament->tgl_tournament : old('tgl_tournament') }}">
                            </div>
                            @if (isset($dataTournament))
                                
                            @else
                                <div class="form-group">
                                    <label for="">Poster Turnamen</label>
                                    <input type="file" name="poster_tournament" class="form-control">
                                </div>
                            @endif
                            
                            <div class="form-group">
                                <label for="">Deskripsi Turnamen</label>
                                <textarea name="deskripsi_tournament" class="form-control" cols="30" rows="5">{{ isset($dataTournament) ? $dataTournament->deskripsi_tournament : old('deskripsi_tournament') }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="">Status</label>
                                <select name="status" class="form-control" value="{{ isset($dataTournament) ? $dataTournament->status : old('status') }}">
                                    <option value="">Pilih Status</option>
                                    <option value="1" {{ isset($dataTournament) ? $dataTournament->status == 1 ? 'selected' : '' : old('status') }}>Aktif</option>
                                    <option value="0" {{ isset($dataTournament) ? $dataTournament->status == 0 ? 'selected' : '' : old('status') }}>Non Aktif</option>
                                </select>
                                <small>Note: Status non aktif turnamen sudah selesai.</small>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection
@section('js')
    <script type="text/javascript">

    </script>
@endsection