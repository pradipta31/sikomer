@extends('admin.layouts.master',['activeMenu' => 'tournaments'])
@section('title','Data Turnamen')
@section('breadcrumb', 'Data Turnamen')
@section('detail_breadcrumb', 'Manajemen Data Turnamen')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
    @include('admin.layouts.breadcrumb')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <a href="{{ route('create:tournament') }}" class="btn btn-md btn-primary" style="margin-bottom: 10px">
                            <i class="fa fa-plus"></i>
                            Tambah Turnamen
                        </a>
                        <div class="table-responsive">
                            <table id="tableTurnamen" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Turnamen</th>
                                        <th>Poster Turnamen</th>
                                        <th>Tanggal Turnamen</th>
                                        <th>History Turnamen</th>
                                        <th>Status</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($tournaments as $t)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{$t->nama_tournament}}</td>
                                            <td>
                                                <a href="#" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#showImage{{$t->id}}">
                                                    Lihat Foto
                                                </a>
                                            </td>
                                            <td><span class="label label-primary">{{date('d M Y', strtotime($t->tgl_tournament))}}</span></td>
                                            <td>
                                                <a href="#" class="btn btn-success btn-sm"><i class="fa fa-history"></i> History</a>
                                            </td>
                                            <td>
                                                @if ($t->status == 1)
                                                    <span class="label label-success">Aktif</span>
                                                @else
                                                    <span class="label label-success">Selesai</span>
                                                @endif
                                            </td>
                                            <td>
                                                <a href="javascript:void(0);" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#showUser{{$t->id}}">
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                                {{-- @if ($user->role != 'admin') --}}
                                                    <a href="{{route('edit:tournament', $t)}}" class="btn btn-sm btn-warning">
                                                        <i class="fa fa-pencil"></i>
                                                    </a>
                                                    <a href="javascript:void(0);" class="btn btn-danger btn-sm" onclick="deleteTournament({{ $t->id }})">
                                                        <i class="fa fa-trash"></i>
                                                    </a>
                                                {{-- @else --}}
                                                    
                                                {{-- @endif --}}
                                            </td>
                                        </tr>
                                        <div class="modal fade" id="showImage{{$t->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <form action="{{url('admin/turnamen/'.$t->id.'/ganti-poster')}}" method="POST" enctype="multipart/form-data">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="_method" value="put">
                                                        <div class="modal-body">
                                                            <img src="{{asset('images/poster/'.$t->poster_tournament)}}" alt="" class="img-responsive img-fluid">
                                                            <label for="">Ganti Poster</label>
                                                            <input type="file" name="poster_tournament" class="form-control">
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                                            <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="modal fade" id="showUser{{$t->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h3 class="modal-title" id="exampleModalLabel">NAMA Tournament : {{$t->nama_tournament}}</h3>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <form action="#" method="post" enctype="multipart/form-data">
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-md-2">
                                                                    <p><b>Nama </b></p>
                                                                    <p><b>Tanggal</b></p>
                                                                    <p><b>Deskripsi</b></p>
                                                                    <p><b>Status</b></p>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <p>: {{$t->nama_tournament}}</p>
                                                                    <p>: {{$t->tgl_tournament}}</p>
                                                                    <p>: {{$t->deskripsi_tournament}}</p>
                                                                    <p>: 
                                                                        @if ($t->status == 1)
                                                                            <span class="label label-success">Aktif</span>
                                                                        @else
                                                                            <span class="label label-warning">Non Aktif</span>
                                                                        @endif
                                                                    </p>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <img src="{{asset('images/poster/'.$t->poster_tournament)}}" alt="" class="img-responsive img-fluid">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <form class="hidden" action="" method="post" id="formDelete">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="delete">
    </form>
@endsection

@section('js')
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tableTurnamen').dataTable()
        });

        function deleteTournament(id){
            swal({
                title: "Anda yakin?",
                text: "Data Tournament akan terhapus secara permanen!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    swal("Berhasil! Data Tournament yang anda pilih berhasil terhapus!", {
                        icon: "success",
                    }).then((res) => {
                        $('#formDelete').attr('action', '{{url('admin/turnamen/delete/')}}/'+id);
                        $('#formDelete').submit();
                    }); 
                }
            });
        }
    </script>
@endsection
