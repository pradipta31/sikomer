@extends('admin.layouts.master',['activeMenu' => 'event'])
@section('title','Data Gallery')
@section('breadcrumb', 'Data Gallery')
@section('detail_breadcrumb', 'Manajemen Data Gallery')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
    @include('admin.layouts.breadcrumb')
    <section class="content">
        <div class="row">
            <div class="col-xs-4">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tambah Foto Baru : {{$event->nama_event}}</h3>
                    </div>
                    <form action="{{url('admin/event/gallery')}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="id_event" value="{{$event->id}}">
                        <div class="box-body">
                            <div class="form-group">
                                <label>Gambar</label>
                                <input type="file" class="form-control" name="photo[]" style="height: 25%;" multiple>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary" onclick="saveBtn(this);">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
          
            <div class="col-xs-8">
                <div class="box">
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="tableGallery" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Event</th>
                                        <th>Foto</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($galleries as $gallery)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{$gallery->event->nama_event}}</td>
                                            <td>
                                                <center>
                                                    <a href="#" data-toggle="modal" data-target="#showImage{{$gallery->id}}">
                                                        <img src="{{Storage::url($gallery->photo)}}" alt="" width="75px" height="75px" class="img-responsive img-bordered">
                                                    </a>
                                                </center>                            
                                            </td>
                                            <td>
                                                <a href="javascript:void(0);" class="btn btn-danger btn-sm" onclick="deletePhoto({{ $gallery->id }})">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <div class="modal fade" id="showImage{{$gallery->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-body">
                                                        <img src="{{Storage::url($gallery->photo)}}" alt="" class="img-responsive img-fluid">
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <form class="hidden" action="" method="post" id="formDelete">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="delete">
    </form>
@endsection

@section('js')
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tableGallery').dataTable()
        });
        function deletePhoto(id){
            swal({
                title: "Anda yakin?",
                text: "Foto akan terhapus secara permanen!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                swal("Berhasil! Foto yang anda pilih berhasil terhapus!", {
                    icon: "success",
                }).then((res) => {
                    $('#formDelete').attr('action', '{{url('admin/gallery/delete')}}/'+id);
                    $('#formDelete').submit();
                }); 
                }
            });
        }

    </script>
@endsection
