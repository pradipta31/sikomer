@extends('admin.layouts.master',['activeMenu' => 'event'])
@section('title','Data Event')
@section('breadcrumb', 'Data Event')
@section('detail_breadcrumb', 'Manajemen Data Event')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
    @include('admin.layouts.breadcrumb')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <a href="{{ route('create:event') }}" class="btn btn-md btn-primary" style="margin-bottom: 10px">
                            <i class="fa fa-plus"></i>
                            Tambah Event
                        </a>
                        <div class="table-responsive">
                            <table id="tableTurnamen" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Event</th>
                                        <th>Cover</th>
                                        <th>Deskripsi</th>
                                        <th>Gallery</th>
                                        <th>Tanggal</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($events as $event)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{$event->nama_event}}</td>
                                            <td>
                                                <a href="#" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#showImage{{$event->id}}">
                                                    Lihat Cover
                                                </a>
                                            </td>
                                            <td>
                                                {{$event->deskripsi}}
                                            </td>
                                            <td>
                                                <a href="{{url('admin/event/'.$event->slug.'/gallery')}}" class="btn btn-success btn-sm"><i class="fa fa-photo"></i> Gallery</a>
                                            </td>
                                            <td><span class="label label-primary">{{date('d M Y', strtotime($event->tanggal))}}</span></td>
                                            <td>
                                                <a href="{{route('edit:event', $event)}}" class="btn btn-sm btn-warning">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                                <a href="javascript:void(0);" class="btn btn-danger btn-sm" onclick="deleteEvent({{ $event->id }})">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <div class="modal fade" id="showImage{{$event->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <form action="{{url('admin/event/'.$event->id.'/ganti-cover')}}" method="POST" enctype="multipart/form-data">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="_method" value="put">
                                                        <div class="modal-body">
                                                            <img src="{{asset('images/event/'.$event->cover)}}" alt="" class="img-responsive img-fluid">
                                                            <label for="">Ganti Cover Event</label>
                                                            <input type="file" name="cover" class="form-control">
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                                            <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <form class="hidden" action="" method="post" id="formDelete">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="delete">
    </form>
@endsection

@section('js')
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tableTurnamen').dataTable()
        });

        function deleteEvent(id){
            swal({
                title: "Anda yakin?",
                text: "Data Event akan terhapus secara permanen, Gallery di dalamnya juga akan ikut terhapus!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    swal("Berhasil! Data Event yang anda pilih berhasil terhapus!", {
                        icon: "success",
                    }).then((res) => {
                        $('#formDelete').attr('action', '{{url('admin/event/delete/')}}/'+id);
                        $('#formDelete').submit();
                    }); 
                }
            });
        }
    </script>
@endsection
