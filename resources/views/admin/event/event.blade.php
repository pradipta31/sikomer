@extends('admin.layouts.master',['activeMenu' => 'event'])
@section('title', isset($dataEvent) ? 'Edit Event' : 'Tambah Event')
@section('breadcrumb', isset($dataEvent) ? 'Edit Event' : 'Tambah Event')
@section('detail_breadcrumb', isset($dataEvent) ? 'Edit Event' : 'Tambah Event')
@section('content')
    @include('admin.layouts.breadcrumb')
    <style>
        select option[disabled] {
            display: none;
        }
    </style>
    <section class="content">
        <form action="{{ isset($dataEvent) ? route('update:event', $dataEvent) : route('store:event') }}" 
        name="formUser" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            {{ isset($dataEvent) ? method_field('PUT') : ''}}
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">{{ isset($dataEvent) ? 'Edit Event' : 'Tambah Event Baru' }}</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="">Nama Event</label>
                                <input type="text" class="form-control" name="nama_event" placeholder="Masukkan Nama Event"
                                value="{{ isset($dataEvent) ? $dataEvent->nama_event : old('nama_event') }}">
                            </div>
                            @if (isset($dataEvent))
                                
                            @else
                                <div class="form-group">
                                    <label for="">Cover Album</label>
                                    <input type="file" class="form-control" name="cover">
                                </div>
                            @endif
                            <div class="form-group">
                                <label for="">Tanggal Event</label>
                                <input type="date" class="form-control" name="tanggal" 
                                value="{{ isset($dataEvent) ? $dataEvent->tanggal : old('tanggal') }}">
                            </div>
                            <div class="form-group">
                                <label for="">Deskripsi Event</label>
                                <textarea name="deskripsi" cols="30" rows="10" class="form-control">
                                    {{ isset($dataEvent) ? $dataEvent->deskripsi : old('deskripsi') }}
                                </textarea>
                            </div>
                        </div>
                        <div class="box-footer">
                            <a href="{{url('admin/event')}}" class="btn btn-default">Kembali</a>
                            <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection