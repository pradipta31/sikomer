@extends('admin.layouts.master',['activeMenu' => 'member'])
@section('title', isset($dataMember) ? 'Edit Member' : 'Tambah Member')
@section('breadcrumb', isset($dataMember) ? 'Edit Member' : 'Tambah Member')
@section('detail_breadcrumb', isset($dataMember) ? 'Edit Member' : 'Tambah Member')
@section('content')
    @include('admin.layouts.breadcrumb')
    <style>
        select option[disabled] {
            display: none;
        }
    </style>
    <section class="content">
        <form action="{{ isset($dataMember) ? route('update:member', $dataMember) : route('store:member') }}" 
        name="formUser" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            {{ isset($dataMember) ? method_field('PUT') : ''}}
            <input type="hidden" name="length" value="6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ isset($dataMember) ? 'Edit Member' : 'Tambah Member Baru' }}</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Nama</label>
                                <input type="text" name="nama" class="form-control"  placeholder="Masukan Nama"
                                value="{{ isset($dataMember) ? $dataMember->user->nama : old('nama') }}">
                            </div>
                            <div class="form-group">
                                <label for="">Username</label>
                                <input type="text" name="username" class="form-control"  placeholder="Masukkan Username"
                                value="{{ isset($dataMember) ? $dataMember->user->username : old('username') }}">
                            </div>
                            <div class="form-group">
                                <label for="">Email</label>
                                <input type="email" name="email" class="form-control" placeholder="Masukkan Email"
                                 value="{{ isset($dataMember) ? $dataMember->user->email : old('email') }}">
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-9">
                                        <label class="col-form-label">Password</label>
                                        <input type="text" class="form-control" name="password" value="{{old('password')}}">
                                        <small>Kosongkan jika tidak ingin mengganti password</small>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="col-form-label"></label>
                                        <button type="button" class="btn btn-success btn-md" onclick="generate();" style="margin-top: 5px">
                                            <i class="fa fa-refresh"></i>
                                            Generate
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Role</label>
                                <select name="role" class="form-control" readonly>
                                    {{-- <option value="admin">Admin</option> --}}
                                    <option value="member">Member</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Status</label>
                                <select name="status" class="form-control" value="{{ isset($dataMember) ? $dataMember->user->status : old('status') }}">
                                    <option value="">Pilih Status</option>
                                    <option value="1" {{ isset($dataMember) ? $dataMember->user->status == 1 ? 'selected' : '' : old('status') }}>Aktif</option>
                                    <option value="0" {{ isset($dataMember) ? $dataMember->user->status == 0 ? 'selected' : '' : old('status') }}>Non Aktif</option>
                                </select>
                                <small>Note: Status aktif dapat melakukan login.</small>
                            </div>
                            
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Nickname</label>
                                <input type="text" name="nickname" class="form-control" placeholder="Masukkan Nickname" 
                                value="{{ isset($dataMember) ? $dataMember->nickname : old('nickname') }}">
                            </div>
                            <div class="form-group">
                                <label for="">Tier</label>
                                <input type="text" name="tier" class="form-control" placeholder="Masukkan Tier" 
                                value="{{ isset($dataMember) ? $dataMember->tier : old('tier') }}">
                            </div>
                            <div class="form-group">
                                <label for="">No Telepon</label>
                                <input type="number" name="no_telp" class="form-control" placeholder="Masukkan Nomor Telepon" 
                                value="{{ isset($dataMember) ? $dataMember->no_hp : old('no_telp') }}">
                            </div>
                            <div class="form-group">
                                <label for="">Alamat</label>
                                <textarea name="alamat" cols="30" rows="9" class="form-control">{{ isset($dataMember) ? $dataMember->alamat : old('alamat') }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan</button>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection
@section('js')
    <script type="text/javascript">
        function randomPassword(length) {
            var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOP1234567890";
            var pass = "";
            for (var x = 0; x < length; x++) {
                var i = Math.floor(Math.random() * chars.length);
                pass += chars.charAt(i);
            }
            return pass;
        }

        function generate() {
            formUser.password.value = randomPassword(formUser.length.value);
        }
    </script>
@endsection