@extends('admin.layouts.master',['activeMenu' => 'member'])
@section('title','Data Member')
@section('breadcrumb', 'Data Member')
@section('detail_breadcrumb', 'Manajemen Data Member')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
    @include('admin.layouts.breadcrumb')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <a href="{{ route('create:member') }}" class="btn btn-md btn-primary" style="margin-bottom: 10px">
                            <i class="fa fa-plus"></i>
                            Tambah Member
                        </a>
                        <div class="table-responsive">
                            <table id="tableMember" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Username</th>
                                        <th>Email</th>
                                        <th>Role</th>
                                        <th>Status</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($users as $user)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{$user->user->nama}}</td>
                                            <td>{{$user->user->username}}</td>
                                            <td>{{$user->user->email}}</td>
                                            <td>
                                                @if ($user->user->role == 'admin')
                                                    <span class="label label-primary">Admin</span>
                                                @else
                                                    <span class="label label-info">Petugas</span>
                                                @endif
                                            </td>
                                            <td>
                                                @if ($user->user->status == 1)
                                                    <span class="label label-success">Aktif</span>
                                                @else
                                                    <span class="label label-warning">Non Aktif</span>
                                                @endif
                                            </td>
                                            <td>
                                                <a href="javascript:void(0);" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#showUser{{$user->id}}">
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                                {{-- @if ($user->role != 'admin') --}}
                                                    <a href="{{route('edit:member', $user)}}" class="btn btn-sm btn-warning">
                                                        <i class="fa fa-pencil"></i>
                                                    </a>
                                                    <a href="javascript:void(0);" class="btn btn-danger btn-sm" onclick="deleteUser({{ $user->id }})">
                                                        <i class="fa fa-trash"></i>
                                                    </a>
                                                {{-- @else --}}
                                                    
                                                {{-- @endif --}}
                                            </td>
                                        </tr>

                                        <div class="modal fade" id="showUser{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h3 class="modal-title" id="exampleModalLabel">NAMA USER : {{$user->nama}}</h3>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <form action="#" method="post" enctype="multipart/form-data">
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-md-3">
                                                                    <p><b>Nama </b></p>
                                                                    <p><b>Username</b></p>
                                                                    <p><b>Email</b></p>
                                                                    <p><b>Nickname</b></p>
                                                                    <p><b>Tier</b></p>
                                                                    <p><b>No Telepon</b></p>
                                                                    <p><b>Alamat</b></p>
                                                                    <p><b>Status</b></p>
                                                                </div>
                                                                <div class="col-md-9">
                                                                    <p>: {{$user->user->nama}}</p>
                                                                    <p>: {{$user->user->username}}</p>
                                                                    <p>: {{$user->user->email}}</p>
                                                                    <p>: {{$user->nickname}}</p>
                                                                    <p>: <span class="label label-primary">{{$user->tier}}</span></p>
                                                                    <p>: {{$user->no_hp}}</p>
                                                                    <p>: {{$user->alamat}}</p>
                                                                    <p>: 
                                                                        @if ($user->user->status == 1)
                                                                            <span class="label label-success">Aktif</span>
                                                                        @else
                                                                            <span class="label label-warning">Non Aktif</span>
                                                                        @endif
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <form class="hidden" action="" method="post" id="formDelete">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="delete">
    </form>
@endsection

@section('js')
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tableMember').dataTable()
        });

        function deleteUser(id){
            swal({
                title: "Anda yakin?",
                text: "Data Member akan terhapus secara permanen!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    swal("Berhasil! Data Member yang anda pilih berhasil terhapus!", {
                        icon: "success",
                    }).then((res) => {
                        $('#formDelete').attr('action', '{{url('admin/member/delete/')}}/'+id);
                        $('#formDelete').submit();
                    }); 
                }
            });
        }
    </script>
@endsection
