<section id="inner-headline">
    <div class="container">
        <div class="row">
            <div class="span12">
                <div class="inner-heading">
                    <ul class="breadcrumb">
                        <li><a href="{{url('/')}}">Home</a> <i class="icon-angle-right"></i></li>
                        <li class="active">@yield('breadcrumb_member')</li>
                    </ul>
                    <h2>@yield('breadcrumb_member')</h2>
                </div>
            </div>
        </div>
    </div>
</section>