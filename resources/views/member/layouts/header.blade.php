<!-- start header -->
<header>
    <div class="top">
      {{-- <div class="container">
        <div class="row">
          <div class="span6">
            <p class="topcontact"><i class="icon-phone"></i> +62 088 999 123</p>
          </div>
          <div class="span6">

            <ul class="social-network">
              <li><a href="#" data-placement="bottom" title="Facebook"><i class="icon-facebook icon-white"></i></a></li>
              <li><a href="#" data-placement="bottom" title="Twitter"><i class="icon-twitter icon-white"></i></a></li>
              <li><a href="#" data-placement="bottom" title="Linkedin"><i class="icon-linkedin icon-white"></i></a></li>
              <li><a href="#" data-placement="bottom" title="Pinterest"><i class="icon-pinterest  icon-white"></i></a></li>
              <li><a href="#" data-placement="bottom" title="Google +"><i class="icon-google-plus icon-white"></i></a></li>
              <li><a href="#" data-placement="bottom" title="Dribbble"><i class="icon-dribbble icon-white"></i></a></li>
            </ul>

          </div>
        </div>
      </div> --}}
    </div>
    <div class="container">


      <div class="row nomargin">
        <div class="span4">
          <div class="logo">
            <a href="index.html"><img src="{{asset('frontend/img/logo.png')}}" alt="" /></a>
          </div>
        </div>
        <div class="span8">
          <div class="navbar navbar-static-top">
            <div class="navigation">
              <nav>
                <ul class="nav topnav">
                  @auth
                    <li class="{{$activeMenu == 'home' ? 'active' : ''}}">
                      <a href="{{('/')}}"><i class="icon-home"></i> Home </a>
                    </li>
                    <li class="{{$activeMenu == 'event' ? 'active' : ''}}">
                      <a href="{{url('daftar-event')}}"> Gallery Event </a>
                    </li>
                    <li class="{{$activeMenu == 'tournament' ? 'active' : ''}}">
                      <a href="{{url('daftar-tournament')}}"> Daftar Turnamen </a>
                    </li>
                    
                    <li>
                      <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="icon-signin"></i> Logout </a>
                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                          @csrf
                      </form>
                    </li>  
                  @else
                    <li class="{{$activeMenu == 'home' ? 'active' : ''}}">
                      <a href="{{url('/')}}"><i class="icon-home"></i> Home </a>
                    </li>
                    <li class="{{$activeMenu == 'event' ? 'active' : ''}}">
                      <a href="{{url('daftar-event')}}"> Gallery Event </a>
                    </li>
                    <li>
                      <a href="{{url('login')}}"><i class="icon-signin"></i> Login/Register </a>
                    </li>
                  @endauth
                </ul>
              </nav>
            </div>
            <!-- end navigation -->
          </div>
        </div>
      </div>
    </div>
  </header>