<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>Sistem Informasi Komunitas AOV</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta name="description" content="Your page description here" />
  <meta name="author" content="" />

  <!-- css -->
  <link href="https://fonts.googleapis.com/css?family=Handlee|Open+Sans:300,400,600,700,800" rel="stylesheet">
  <link href="{{asset('frontend/css/bootstrap.css')}}" rel="stylesheet" />
  <link href="{{asset('frontend/css/bootstrap-responsive.css')}}" rel="stylesheet" />
  <link href="{{asset('frontend/css/flexslider.css')}}" rel="stylesheet" />
  <link href="{{asset('frontend/css/prettyPhoto.css')}}" rel="stylesheet" />
  <link href="{{asset('frontend/css/camera.css')}}" rel="stylesheet" />
  <link href="{{asset('frontend/css/jquery.bxslider.css')}}" rel="stylesheet" />
  <link href="{{asset('frontend/css/cslider.css')}}" rel="stylesheet" />
  <link href="{{asset('frontend/css/style.css')}}" rel="stylesheet" />

  <!-- Theme skin -->
  <link href="{{asset('frontend/color/default.css')}}" rel="stylesheet" />

  <!-- Fav and touch icons -->
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{asset('frontend/ico/apple-touch-icon-144-precomposed.png')}}" />
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{asset('frontend/ico/apple-touch-icon-114-precomposed.png')}}" />
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{asset('frontend/ico/apple-touch-icon-72-precomposed.png')}}" />
  <link rel="apple-touch-icon-precomposed" href="{{asset('frontend/ico/apple-touch-icon-57-precomposed.png')}}" />
  <link rel="shortcut icon" href="{{asset('frontend/ico/favicon.png')}}" />
  @toastr_css
</head>

<body>

  <div id="wrapper">
    @include('member.layouts.header', ['activeMenu' => $activeMenu])
    @yield('contentmember')
    @include('member.layouts.footer')
  </div>
  <a href="#" class="scrollup"><i class="icon-angle-up icon-square icon-bglight icon-2x active"></i></a>
  <script src="{{asset('frontend/js/jquery.js')}}"></script>
  <script src="{{asset('frontend/js/jquery.easing.1.3.js')}}"></script>
  <script src="{{asset('frontend/js/bootstrap.js')}}"></script>

  <script src="{{asset('frontend/js/modernizr.custom.js')}}"></script>
  <script src="{{asset('frontend/js/toucheffects.js')}}"></script>
  <script src="{{asset('frontend/js/google-code-prettify/prettify.js')}}"></script>
  <script src="{{asset('frontend/js/jquery.bxslider.min.js')}}"></script>
  <script src="{{asset('frontend/js/camera/camera.js')}}"></script>
  <script src="{{asset('frontend/js/camera/setting.js')}}"></script>

  <script src="{{asset('frontend/js/jquery.cslider.js')}}"></script>

  <script src="{{asset('frontend/js/jquery.prettyPhoto.js')}}"></script>
  <script src="{{asset('frontend/js/portfolio/jquery.quicksand.js')}}"></script>
  <script src="{{asset('frontend/js/portfolio/setting.js')}}"></script>

  <script src="{{asset('frontend/js/jquery.flexslider.js')}}"></script>
  <script src="{{asset('frontend/js/animate.js')}}"></script>
  <script src="{{asset('frontend/js/inview.js')}}"></script>
  <script>
    $(function() {
      $('#da-slider').cslider();
    });
  </script>

  <!-- Template Custom JavaScript File -->
  <script src="{{asset('frontend/js/custom.js')}}"></script>
  @jquery
  @toastr_js
  @toastr_render
</body>
</html>
