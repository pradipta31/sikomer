@extends('member.layouts.master',['activeMenu' => 'tournament'])
@section('contentmember')
@section('breadcrumb_member', 'Daftar Tournament')
    @include('member.layouts.breadcrumb')
    <section id="content">
        <div class="container">
          <div class="row">
            <div class="span12">
              @foreach ($tournaments as $item)
                <article>
                    <div class="row">
                        <div class="span8">
                            <div class="post-image">
                                <div class="post-heading">
                                    <h3><a href="#">{{$item->nama_tournament}}</a></h3>
                                </div>
                                <img src="{{asset('images/poster/'.$item->poster_tournament)}}" alt="" />
                            </div>
                        </div>
                        <div class="span4">
                            <div class="meta-post">
                                <ul>
                                    <li><i class="icon-file"></i></li>
                                    <li>By <a href="#" class="author">Admin</a></li>
                                    <li>On <a href="#" class="date">{{$item->created_at}}</a></li>
                                </ul>
                            </div>
                            <div class="post-entry">
                                <p>
                                    {{$item->deskripsi_tournament}}
                                </p>
                                <a href="{{url('daftar-tournament/daftar/'.$item->id)}}" class="btn btn-primary btn-rounded">DAFTAR SEKARANG <i class="icon-angle-right"></i></a>
                            </div>
                        </div>
                    </div>
                </article>
              @endforeach
              <div id="pagination">
                <span class="all">Page 1 of 3</span>
                <span class="current">1</span>
                <a href="#" class="inactive">2</a>
                <a href="#" class="inactive">3</a>
              </div>
            </div>
          </div>
        </div>
      </section>
@endsection