@extends('member.layouts.master',['activeMenu' => 'tournament'])
@section('contentmember')
@section('breadcrumb_member', 'Pendaftaran Turnamen '.$tournament->nama_tournament)
    @include('member.layouts.breadcrumb')
    <section id="content">

        <div class="container">
            <div class="row">
                <div class="span10">
                    @if ($register->tournament_id == $tournament->id)
                    @php
                        $acak = rand(2, 4);
                        switch($acak){
                            case 1: $image_file = "hero1.jpg";
                            break;
                            case 2: $image_file = "hero2.jpg";
                            break;
                            case 3: $image_file = "hero3.jpg";
                            break;
                            case 4: $image_file = "hero6.jpg";
                            break;
                        }
                    @endphp
                    <h4>Anda sudah terdaftar pada tournament ini!</h4>
                    <h1>Nama Team: {{$register->nama_team}}</h1>
                    <hr>
                    <section id="team">
                        <ul id="thumbs" class="team">
                            <li class="item-thumbs span3 dev" data-id="id-1" data-type="dev">
                                <div class="team-box thumbnail">
                                    <img src="{{asset('images/hero/hero1.jpg')}}" alt="" />
                                    <div class="caption">
                                        <h5>{{$register->captain}}</h5>
                                        <p>
                                            Captain
                                        </p>
                                    </div>
                                </div>
                            </li>
                            <li class="item-thumbs span3 dev" data-id="id-1" data-type="dev">
                                <div class="team-box thumbnail">
                                    <img src="{{asset('images/hero/'.$image_file)}}" alt="" />
                                    <div class="caption">
                                        <h5>{{$register->anggota_1}}</h5>
                                        <p>
                                            Anggota 1
                                        </p>
                                    </div>
                                </div>
                            </li>
                            <li class="item-thumbs span3 dev" data-id="id-1" data-type="dev">
                                <div class="team-box thumbnail">
                                    <img src="{{asset('images/hero/'.$image_file)}}" alt="" />
                                    <div class="caption">
                                        <h5>{{$register->anggota_2}}</h5>
                                        <p>
                                            Anggota 2
                                        </p>
                                    </div>
                                </div>
                            </li>
                            <li class="item-thumbs span3 dev" data-id="id-1" data-type="dev">
                                <div class="team-box thumbnail">
                                    <img src="{{asset('images/hero/'.$image_file)}}" alt="" />
                                    <div class="caption">
                                        <h5>{{$register->anggota_3}}</h5>
                                        <p>
                                            Anggota 3
                                        </p>
                                    </div>
                                </div>
                            </li>
                            <li class="item-thumbs span3 dev" data-id="id-1" data-type="dev">
                                <div class="team-box thumbnail">
                                    <img src="{{asset('images/hero/'.$image_file)}}" alt="" />
                                    <div class="caption">
                                        <h5>{{$register->anggota_4}}</h5>
                                        <p>
                                            Anggota 4
                                        </p>
                                    </div>
                                </div>
                            </li>
                            <li class="item-thumbs span3 dev" data-id="id-1" data-type="dev">
                                <div class="team-box thumbnail">
                                    <img src="{{asset('images/hero/'.$image_file)}}" alt="" />
                                    <div class="caption">
                                        <h5>{{$register->anggota_5}}</h5>
                                        <p>
                                            Anggota 5
                                        </p>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <a href="#myModal" role="button" class="btn btn-large btn-theme" data-toggle="modal">
                            <i class="icon-group"></i>
                            List Team Terdaftar
                        </a>

                        <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h3 id="myModalLabel">All Team: {{$tournament->nama_tournament}}</h3>
                            </div>
                            <div class="modal-body">
                                
                                <div class="row">
                                    @foreach ($reg as $i)
                                        <div class="span2">
                                            <button class="btn btn-default"> {{$i->nama_team}}</button>
                                        </div>
                                    @endforeach
                                </div>
                                <hr>
                                <h4>Total: {{count($reg)}}</h4>
                            </div>
                            <div class="modal-footer">
                                <button class="btn" data-dismiss="modal" aria-hidden="true">Kembali</button>
                            </div>
                        </div>
                    </section>
                    @else
                    <h4>Isilah form berikut dengan benar</h4>

                        <div id="sendmessage">Your message has been sent. Thank you!</div>
                        <div id="errormessage"></div>
                        <form action="{{url('daftar-tournament/daftar')}}" method="post" role="form" class="contactForm">
                            @csrf
                            <input type="hidden" name="tournament_id" value="{{$tournament->id}}">
                            <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                            <div class="row">
                                <div class="span8 form-group field">
                                    <label for="">Nama Team</label>
                                    <input type="text" name="nama_team" id="name" placeholder="Masukkan nama team" value="{{old('nama_team')}}"/>
                                    <div class="validation"></div>
                                </div>
                                <div class="span8 form-group field">
                                    <label for="">Nama Kapten Tim</label>
                                    <input type="text" name="captain" id="name" placeholder="Masukkan nama kapten tim" value="{{old('captain')}}"/>
                                    <div class="validation"></div>
                                </div>

                                <div class="span3 form-group field">
                                    <label for="">Nickname Anggota 1</label>
                                    <input type="text" name="anggota_1" id="name" placeholder="Masukkan nama anggota" value="{{old('anggota_1')}}"/>
                                    <div class="validation"></div>
                                </div>
                                <div class="span3 form-group field">
                                    <label for="">Nickname Anggota 2</label>
                                    <input type="text" name="anggota_2" id="name" placeholder="Masukkan nama anggota" value="{{old('anggota_2')}}"/>
                                    <div class="validation"></div>
                                </div>
                                <div class="span3 form-group field">
                                    <label for="">Nickname Anggota 3</label>
                                    <input type="text" name="anggota_3" id="name" placeholder="Masukkan nama anggota" value="{{old('anggota_3')}}"/>
                                    <div class="validation"></div>
                                </div>
                                <div class="span3 form-group field">
                                    <label for="">Nickname Anggota 4</label>
                                    <input type="text" name="anggota_4" id="name" placeholder="Masukkan nama anggota" value="{{old('anggota_4')}}"/>
                                    <div class="validation"></div>
                                </div>
                                <div class="span2 form-group field">
                                    <label for="">Nickname Anggota 5</label>
                                    <input type="text" name="anggota_5" id="name" placeholder="Masukkan nama anggota" value="{{old('anggota_5')}}"/>
                                    <div class="validation"></div>
                                </div>

                                <div class="span8 form-group field">
                                    <label for="">Nomor Telepon</label>
                                    <input type="text" name="no_hp" id="name" placeholder="Masukkan nomor hp" value="{{old('no_hp')}}"/>
                                    <small>Nomor telepon wajib terkoneksi dengan whatsapp!</small>
                                    <div class="validation"></div>
                                </div>

                                <div class="span8 form-group">
                                    <div class="text-center">
                                        <button class="btn btn-theme btn-medium margintop10" type="submit">Kirim</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    
                    @endif
                </div>
                <div class="span2">
                    <div class="clearfix"></div>
                    <aside class="right-sidebar">

                    <div class="widget">
                        <h5 class="widgetheading">Contact information<span></span></h5>

                        <ul class="contact-info">
                        <li><label>Address :</label> Tinggi sekali tower Jl.Kemacetan timur<br /> Jakarta selatan - Indonesia</li>
                        <li><label>Phone :</label>+62 123 456 78 / +62 123 456 79</li>
                        <li><label>Fax : </label>+62 123 456 10 / +62 123 456 11</li>
                        <li><label>Email : </label> info@yourdomain.com</li>
                        </ul>

                    </div>
                    </aside>
                </div>
            </div>
        </div>
    </section>
@endsection