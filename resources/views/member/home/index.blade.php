@extends('member.layouts.master',['activeMenu' => 'home'])
@section('contentmember')
    @include('member.layouts.carousel')
    @include('member.layouts.content')
@endsection