@extends('member.layouts.master',['activeMenu' => 'event'])
@section('contentmember')
@section('breadcrumb_member', 'Daftar Event')
@include('member.layouts.breadcrumb')
    <section id="content">
        <div class="container">
            <div class="row">

                <div class="span12">
                    <h4>About our company</h4>
                    <p>
                        Ei mel semper vocent persequeris, nominavi patrioque vituperata id vim, cu eam gloriatur philosophia deterruisset. Meliore perfecto repudiare ea nam, ne mea duis temporibus. Id quo accusam consequuntur, eum ea debitis urbanitas. Nibh reformidans vim ne.
                    </p>
                    <blockquote>
                        Iudico definiebas eos ea, dicat inermis hendrerit vel ei, legimus copiosae quo at. Per utinam corrumpit prodesset te, liber praesent eos an. An prodesset neglegentur qui, usu ancillae posidonium in, mea ex eros animal scribentur. Et simul fabellas sit.
                        Populo inimicus ne est.
                    </blockquote>
                </div>
            </div>
            <div class="row">
                <div class="span12">
                    <div class="solidline"></div>
                </div>
            </div>
            <!-- end divider -->

            <div class="row team">
                <div class="span12">
                    <h4 class="title">Daftar Event <strong>AOV Mobile Esports</strong></h4>
                </div>

                @foreach ($events as $event)
                    <div class="span6">
                        <div class="team-box">
                            <a href="{{url('daftar-event/'.$event->slug)}}" class="thumbnail"><img src="{{asset('images/event/'.$event->cover)}}" alt="" /></a>
                            <div class="roles aligncenter">
                                <p class="lead"><strong>{{$event->nama_event}}</strong></p>
                                <p>{{$event->deskripsi}}</p>
                                <p>
                                    <a href="{{url('daftar-event/'.$event->slug)}}" class="btn btn-theme">Lihat Gallery</a>
                                </p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="blankline30"></div>

        </div>
    </section>
@endsection