@extends('member.layouts.master',['activeMenu' => 'event'])
@section('contentmember')
@section('breadcrumb_member', 'Event '.$event->nama_event)
@include('member.layouts.breadcrumb')
<section id="content">
    <div class="container">
      <div class="row">

        <div class="span12">
          <h4>{{$event->nama_event}}</h4>
          <p>
            {{$event->deskripsi}}
          </p>

        </div>

      </div>

      <!-- divider -->
      <div class="row">
        <div class="span12">
          <div class="solidline"></div>
        </div>
      </div>
      <!-- end divider -->

      <div class="row team">
        <div class="span12">
          <h4 class="title">Gallery <strong>Photo</strong></h4>
        </div>

        @foreach ($galleries as $gallery)
            <div class="span3">
                <a href="{{Storage::url($gallery->photo)}}" target="_blank" class="thumbnail"><img src="{{Storage::url($gallery->photo)}}" alt="" /></a>
            </div>
        @endforeach
      </div>
      <div class="blankline30"></div>

    </div>
  </section>
@endsection