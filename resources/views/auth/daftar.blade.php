<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SIKOMER | Log in</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{asset('backend/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/font-awesome/css/font-awesome.min.css')}}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/Ionicons/css/ionicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/dist/css/AdminLTE.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/plugins/iCheck/square/blue.css')}}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    @toastr_css
</head>
<body class="hold-transition login-page">
    <div class="row" style="margin-top: 20px; margin-bottom: -100px">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <div class="login-logo">
                {{-- <img src="{{asset('images/logo.jpeg')}}" alt="" style="width: 20%"> --}}
                <br>
                <a href="{{url('login')}}">
                    <b>SISTEM INFORMASI MANAJEMEN KOMUNITAS GAMER AOV DI BALI</b></a>
                    <br>
                <a href="#">Daftar</a>
            </div>
        </div>
        <div class="col-md-2"></div>
    </div>
<div class="login-box">
    
    <div class="login-box-body">
        <p class="login-box-msg">Silahkan isi data diri anda sebagai berikut</p>
        <form action="{{url('member/daftar')}}" method="POST">
        @csrf
            <div class="form-group has-feedback">
                <input type="text" class="form-control" placeholder="Nama Lengkap" name="nama" value="{{old('nama')}}">
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="text" class="form-control" placeholder="Username" name="username" value="{{old('username')}}">
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="email" class="form-control" placeholder="Email" name="email" value="{{old('email')}}">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" class="form-control" placeholder="Password" name="password" id="password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" class="form-control" placeholder="Retype password" name="confirm_password" id="confirm_password">
                <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                <span id="message"></span>
            </div>
            <hr>
            <div class="form-group has-feedback">
                <input type="text" class="form-control" placeholder="Nickname Game" name="nickname" value="{{old('nickname')}}">
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="text" class="form-control" placeholder="Tier" name="tier" value="{{old('tier')}}">
                <span class="glyphicon glyphicon-triangle-top form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="text" class="form-control" placeholder="Alamat" name="alamat" value="{{old('alamat')}}">
                <span class="glyphicon glyphicon-home form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="text" class="form-control" placeholder="No Hp" name="no_hp" value="{{old('no_hp')}}">
                <span class="glyphicon glyphicon-phone form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-8">
                    <a href="{{url('login')}}" class="btn btn-default"><< Kembali</a>
                </div>
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block" onclick="submitBtn(this);">
                    Daftar >>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="{{asset('backend/bower_components/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('backend/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{asset('backend/plugins/iCheck/icheck.min.js')}}"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%'
    });
  });
  function submitBtn(it){
    it.innerHTML = '<i class="fa fa-spinner fa-spin"></i>';
  }

  $('#confirm_password').on('keyup', function(){
        if($(this).val() == $('#password').val()){
            $('#message').html('Password sama').css('color', 'green');
        }else{
            $('#message').html('Password tidak sama').css('color', 'red');
        }
    })
</script>
@jquery
@toastr_js
@toastr_render
</body>
</html>
